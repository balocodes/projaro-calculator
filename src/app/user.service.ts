import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  sayHello(name) {
    alert(`Goodbye ${name}`);
  }

  getData(){
    return this.http.get("http://localhost:3000/test");
  }
}
