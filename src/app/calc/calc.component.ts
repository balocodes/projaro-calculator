import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "../user.service";

@Component({
  selector: "app-calc",
  templateUrl: "./calc.component.html",
  styleUrls: ["./calc.component.css"],
  providers: [UserService]
})
export class CalcComponent implements OnInit {
  init = "0";
  current_number = "";
  answer = 0;
  last_pressed = "";
  title = "Super Calculator"
  constructor(private my_router: Router, private my_service: UserService) {}

  ngOnInit() {
    this.my_service.getData().subscribe(
      res => {
        console.log(res)

      }, err => {
        console.log(err)
      }
    )
  }

  switchPage() {
    this.my_service.sayHello("Amin")
    this.my_router.navigateByUrl("about");
  }

  concat(num) {
    if((this.last_pressed == (("+" || "-") || ("/" || "x"))) && (num == ("+" || "-" || "/" || "x") )){
      return
    } else {
      console.log(this.last_pressed == ("+" || "-" || "/" || "x"))
      this.last_pressed = num;
      if (this.init == "0") {
        this.init = "";
        if(num == ("+" || "")){
          return
        }
      }
      this.init += num;
      if( num == ("+" || "-" || "/" || "x")){

      } else {
        this.current_number += num;
      }

      console.log(this.current_number)
    }


  }

  operate(operator) {
    switch (operator) {
      case "+":
        this.answer += Number(this.current_number)
        console.log(this.answer)
        break;
      case "-":
        break;
      case "/":
        break;
      case "*":
        break;
      case "c":
        break;

      default:

        break;
    }
  }
}
